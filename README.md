# ipv6-dns-proxy

Inspired by https://gitlab.com/miyurusankalpa/IPv6-dns-server, I wanted to create a python script to integrate into unbound.

The aim of this script (similar to its nodejs inspiration) is to provide the hidden ipv6 addresses of services and websites hosted in CDN based on patterns.

This script uses dnspython.
