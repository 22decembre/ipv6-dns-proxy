#!/usr/bin/env python3

import dns.resolver
import dns.message
import dns.query
import dns.ipv6
import ipaddress
#import asyncore

import pickle

import whois

import sys

name = sys.argv[1]
#print(name)

ip_cdn = ""
dns_cdn = ""

class host():
    def __init__(self, hostname, a, aaaa):
        self.aaaa   = aaaa
        self.a      = a
        self.hostname = hostname


def cache_add(host):
    try:
        with open("data.pickle", "wb") as f:
            pickle.dump(host, f, protocol=pickle.HIGHEST_PROTOCOL)
    except Exception as ex:
        print("Error during pickling object (Possibly unsupported):", ex)



#def import_cache(hostname):



def get_records(domain,qtype):
    try:
        answers = dns.resolver.resolve(domain, qtype)
        for rdata in answers:
            #print(rdata)
            return str(rdata)
    except dns.resolver.NoAnswer:
        print(f"There is no answer to the {qtype} query.")
    except dns.resolver.NXDOMAIN:
        print(f"The domain {domain} does not exist")
    except Exception as e:
        print(f"An error occurred: {e}")


def generate_aaaa(hostname, ipv6):
    if not ipv6:
        return False
    newaaaa = {
        'name': hostname,
        'type': 28,
        'class': 1,
        'ttl': 300,
        'address': ipv6
    }
    # print(newaaaa)
    return newaaaa

######## Microsoft

def msedge_v4tov6(hostname):
    if not hostname:
        return False

    v4 = get_records(hostname,'A')
    
    #print("host :", hostname)
    octets = v4.split(".")
    mseid = hostname.split("-")
    msloc = mseid[0]
    #print("ms", msloc)
    
    #match ...  case please ?
    # require python > 3.10
    match msloc:
        case "a":
            mse_range = '2620:1ec:c11::'
        case "b":
            mse_range = '2620:1ec:a92::'
        case "c":
            mse_range = '2a01:111:2003::'
        case "l":
            mse_range = '2620:1ec:21::'
        case "s":
            mse_range = '2620:1ec:6::'
        case "k":
            mse_range = '2620:1ec:c::'
        case "t":
            mse_range = '2620:1ec:bdf::'
        case "spo":
            mse_range = '2620:1ec:8f8::'
            if (octets[3] == 9):
                octets[3] = 8
        case _:
                return False
    
    return mse_range + octets[3]

######## Weebly

def get_weebly_hostname(hostname):
    if not hostname:
        return False
    
    sdomains = hostname.split(".")
    dp1 = sdomains.index("com")
    dp2 = sdomains.index("weebly")

    if sdomains[-2] == 'com' and sdomains[-3] == 'weebly':
        fixedhostname = ".".join(sdomains)
        return fixedhostname
    else:
        return False

def get_weebly_aaaa():
    return '2620:11c:1:e4::36'

########## Fastly

def fastly_v4tov6(ipv4, cust):
    if not ipv4 or not ipv4[0]:
        return False

    if not cust:
        return False

    print(ipv4,cust)
    octets = ipv4.split(".")

    v6hex = ""
    
    if cust == "fastly":
        addresses = get_records('dualstack.g.shared.global.fastly.net','AAAA')
        v6_range = addresses[:-3]
        print(v6_range)
        
        if len(ipv4) == 1:
            x = int((int(octets[2]) % 4) * 256 + int(octets[3]))
        else:
            x = int((int(octets[2]) % 64) * 256 + int(octets[3]))
        v6hex = hex(x).lstrip('0x').upper()
    
    if cust == "github":
        v6_range = "2606:50c0:8000::"
        v6hex = octets[3]
    
    #aaaa = ipaddress.IPv6Address(v6_range + v6hex).compressed
    #print(aaaa)
    #return aaaa
    return ipaddress.IPv6Address(v6_range + v6hex).compressed

def check_for_fastly_a(authority):
    if not authority:
        return False
    if authority == 'hostmaster.fastly.com':
        return True
    else:
        return False

def check_for_fastly_hostname(hostname):
    if not hostname:
        return False
    sdomains = hostname.split(".")
    sdomains.reverse()
    dp1 = sdomains.index("net")
    dp2 = sdomains.index("fastly")
    dp3 = sdomains.index("fastlylb")

    if dp1 == 0 and (dp2 == 1 or dp3 == 1):
        sdomains.append("dualstack")
        fixedhostname = ".".join(sdomains[::-1])
        return fixedhostname
    else:
        return False


def check_for_stackexchange_ip(ipv4):
    if not ipv4:
        return False

    octets = ipv4.split(".")
    return octets[3] == "69"

########### Edgecast / Azure

def get_edgecast_hostname(hostname):
    if not hostname:
        return False
    else:
        sdomains = hostname.split(".")
        sdomains[-4] = 'cs21'
        fixedhostname = ".".join(sdomains)
        return fixedhostname

######### Wordpress

def decimalToHex(d, padding=2):
    hex_value = hex(int(d))[2:]
    hex_value = hex_value.rjust(padding, "0")
    return hex_value

def wpvip_v4tov6(ipv4,hostname):
    if not ipv4:
        return False
    else:
        octets = ipv4.split(".")
        wpvip_range = '2a04:fa87:fffd::'
        return ipaddress.IPv6Address(wpvip_range + ipv4).compressed
                
        #last_hex = decimalToHex(octets[0]) + decimalToHex(octets[1]) + ":" + decimalToHex(octets[2]) + decimalToHex(octets[3])
        #return wpvip_range + last_hex

#############

def get_aut(name):
    soa = get_records(dns.resolver.zone_for_name(name),'SOA')
    #server = soa.split(" ")[0]
    
    # check on cdn77, cloudflare, and others
    admin = soa.split(" ")[1]
    return admin

def get_v6(name):
    if not name:
        return False
    
    returned_v6 = get_records(name,'AAAA')
    if returned_v6:
        print("no need to think", returned_v6)
        return returned_v6

    #ip_cdn = ""
    #dns_cdn = ""
    if not returned_v6:
        legacy_add = get_records(name,'A')
        
        ## We will return ip directly
        #
        fastly_ranges = ["151.101.0.0/16", "199.232.0.0/16", "146.75.0.0/17", "167.82.0.0/17"]
        for range in fastly_ranges:
            if ipaddress.ip_address(legacy_add) in ipaddress.ip_network(range):
                fastly_v4tov6(legacy_add, "fastly")
                
        if ipaddress.ip_address(legacy_add) in ipaddress.ip_network('192.0.66.0/24'):
            wpvip_v4tov6(legacy_add,name)
        
        if ipaddress.ip_address(legacy_add) in ipaddress.ip_network('185.199.108.0/22'):
            fastly_v4tov6(legacy_add, "github")
        
        if ipaddress.ip_address(legacy_add) in ipaddress.ip_network('199.34.228.0/22'):
            ip_cdn = 'weebly'
        if ipaddress.ip_address(legacy_add) in ipaddress.ip_network("192.124.249.0/24"):
            ip_cdn = 'sucuri'
        if ipaddress.ip_address(legacy_add) in ipaddress.ip_network("104.16.0.0/12"):
            ip_cdn = 'cloudflare'
        
        ## We will find the ipv6 hostname
        ## then resolve it to get the ip
        #
        name.rstrip(".")
        sdomains = name.split(".")
        #sdomains.reverse()

        match sdomains[-1]:
            case "net":
                # fastly, msedgde, akamey
                if "msedge" in sdomains[-2]:
                    msedge_v4tov6(name)
                    #dns_cdn = 'msedge'
                    
                else:
                    match sdomains[-2]:
                        case "fastly"|"fastlylb":
                            dns_cdn = 'fastly'
                        
                        case "b-cdn.net":
                            dns_cdn = 'bunny'
                        case "hwcdn":
                            dns_cdn = 'highwinds'
                            
                        case "v0cdn":
                            sdomains[-3] = 'cs21'
                            fixedhostname = ".".join(sdomains)
                        
                        
                        case "cloudfront":
                            dns_cdn = 'cloudfront'
                        case "akamaiedge"|"akamai":
                            if sdomains[-3] == 'cj':
                                    sdomains[-3] = 'ds' + sdomains[-3]
                            else:
                                    sdomains[-3] = 'dsc' + sdomains[-3]
                            
                            fixedhostname = ".".join(sdomains)
                            #print(fixedhostname)
                            
                            
                        case "v0cdn":
                            dns_cdn = "edgecast"
                        case _:
                            return False
        
            case "org":
                if sdomains[1] == "cdn77":
                    dns_cdn = "cdn77"
            case "cn":
                if sdomains[1] == "amazonaws":
                    dns_cdn = "awss3"
            case "com":
                match sdomains[1]:
                    case "amazonaws":
                        dns_cdn = "awss3"
                    case "stackpathcdn":
                        dns_cdn = 'highwinds'
                    case "fastly":
                        dns_cdn = 'fastly'
                    case _:
                        return False
                return False
        
            case _:
                return False
            
    print("aaaa host", fixedhostname)
    #print("dns :", dns_cdn, "ip :", ip_cdn)

#for question in 'SOA','CNAME','A', 'AAAA':
#    get_records(name,question)

#print(generate_aaaa(name, get_v6(name)))

print(get_v6(name))
    

